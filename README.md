# Gestion du SIA

Repo servant à faire la gestion du SIA. Toute la gestion est définie dans les issues.

# Liste des membres SIA

Il existe 3 niveaux d’accréditation au SIA : 
- **Contributeur** : Les contributeurs sont des personnes participant à un ou plusieurs projets du SIA sans pour autant en faire partie de manière permanente.
- **Maintainer** : Les maintainers sont des contributeurs responsables des projets. Ils valident les mises à jour des dépôts. Ils
doivent être validés par le Bureau du SIA et signataire de la charte maintainer.
- **Administrateur** : Les administrateurs sont validés par le CA du BdE et par le CVA. Ils ont accès aux données et sont en charge de l’infrastructure. Ils doivent être signataire de la charte d’administrateur. 

Le bureau de Service Informatique Interassociatif est constitué de : 

 - Alban Prats (4TC) : Désigné par les membres du SIA lors de la réunion du 12/02/2020
 - Mathéo Atché (4IF) : Désigné par le BdE lors du conseil d'administration du 11/02/2020
 - Pierre-Yves Tardieu (4GM) : Désigné par le CVA lors de la réunion du 17/02/2020

## Liste au 14/02/2020

|Identifiant | Statut  |
| --|--|
| @al26p | Administrateur |
| @Xia0ben | Maintainer |
| @corentinberthier | Administrateur |
| @Xia0ben | Contributeur |
| @EnzoZatt | Contributeur |
| @JeanRibes | Administrateur |
| @GuiYom | Contributeur |
| @firemat | Maintainer |
| @kaloyan-veselinov | Maintainer |
| @Lgtx  | Contributeur |
| @PhilippeVienne  | Maintainer |
| @hugothomas  | Contributeur |
| @pytardieu  | Maintainer |
