# Bug dans une application

## Sommaire
> Rapide description du bug rencontré.



## Etapes pour reproduire le bug
> Décrivez comment obtenir le bug.



## Résultat attendu
> Décrivez le résulat que vous souhaitiez obtenir lorsque vous avez rencontré le bug.



## Résulta obtenu 
> Décrivez le resutat que vous avez obtenu à la place.



## Notes
> Ajoutez tous les éléments que vous pensez nécessaire.


## Quick actions
<!---Ne modifiez ce paragraphe que si vous savez ce que vous faites. Ces quick actions sont là pour faire en sorte que les issues soient traitées de manière la plus efficace.--->
/label Priorité::haute
/label Bug

