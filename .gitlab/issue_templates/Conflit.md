# Gestion d'un confit
Gestion d'un confit entre membres du SIA ou avec un client

## Personnes impliquées
> Indique la.e.es personne.s impliqué.e.s dans ce confilt.

Syntaxe : `/assign @user1 @user2`

/assign <Indiquez ici les personnes concernées>

## Contexte
> Explique nous ce qu'il s'est passé.




## Quick actions
<!---Ne modifiez ce paragraphe que si vous savez ce que vous faites. Ces quick actions sont là pour faire en sorte que les issues soient traitées de manière la plus efficace.--->

/confidential
/assign me /assign @al26p @pytardieu
/label Conflit
