# Problème avec l'infra gérée par le SIA
<!---Si le site affiche une erreur 404 ou 50X, le problème vient sûrement du serveur en lui même. Ce formulaire est dédié aux problèmes matériels mais pas aux bugs logiciels--->
## Sommaire
> Rapide description du bug rencontré.



## Equipement concerné
> Indiquez l'équipement concerné par la défaillance ainsi que son lieu si besoin.



## Contexte
> Décrivez ce que vous essayiez de faire quand vous avez remarqué le problème



## Notes
> Ajoutez tous les éléments que vous pensez nécessaire.



## Quick actions
<!---Ne modifiez ce paragraphe que si vous savez ce que vous faites. Ces quick actions sont là pour faire en sorte que les issues soient traitées de manière la plus efficace.--->

/label Infra
