# Demandes de droits
<!---Demandes d'accès, de badges, de permissions admin, de statuts gitlab...--->

## Personnes impliquées
> Indique la.e.es personne.s impliqué.e.s dans ce confilt.

Syntaxe : `/assign @user1 @user2`

/assign <Indiquez ici les personnes concernées>

## Besoin
> Explique nous ce qu'il s'est passé.




## Quick actions
<!---Ne modifiez ce paragraphe que si vous savez ce que vous faites. Ces quick actions sont là pour faire en sorte que les issues soient traitées de manière la plus efficace.--->

/assign me /assign @al26p @pytardieu 
/label Gestion::Droits
